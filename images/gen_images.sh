#!/bin/bash

# Generates base images per version.
# Walkes folders matching'v*' and builds their respectiv docker context.
# Before that, calls gen_context in order the build contexts

set -Eeuo pipefail


set +u
if [ -z "${1+x}" ]; then
	echo -e "Usage: gen_images IMAGEREPO"
	echo -e ""
	echo -e "Use IMAGEREPO in the form repo/image without tags."
	echo -e "Tags will be created by the scripts as per app version."
fi
set -u


DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"


source "${DIR}"/gen_contexts.sh

if [ $2 == 'ci' ]; then
	docker build --tag "${1}:${2}" "ci-image"
	docker push "${1}:${2}"
else
	for path in $(find "${DIR}" -maxdepth 1 -type d -name 'v-*') ; do
		name=$(basename "${path}")
		version=${name#"v-"}
		if [[ $name =~ $2 ]]; then
			docker build --tag "${1}:${version}-base" "${name}/out/base"
			docker push "${1}:${version}-base"
			docker build --tag "${1}:${version}-devops" "${name}/out/devops"
			docker push "${1}:${version}-devops"
		fi
	done
fi
